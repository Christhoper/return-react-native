import React, { Component } from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input } from 'native-base';
import styles from '../../Login/styles/styleLogin'

//Se pueden agregar varios iconos de esta forma import { FontAwesome, Ionicons } from '@expo/vector-icons';
class Return extends Component {
    render() {
        return (
            <Container>
                <Content padder contentContainerStyle={styles.content}>
                    <KeyboardAvoidingView behavior="padding" enabled>
                        <Card>
                            <CardItem header bordered>
                                <Text style={styles.textcenter}>Bienvenido</Text>
                            </CardItem>
                        </Card>
                    </KeyboardAvoidingView>
                </Content>
            </Container>
        );
    }
}

export default Return