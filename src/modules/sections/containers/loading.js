import React, { Component } from 'react';
import { View, AsyncStorage} from 'react-native';
import { Spinner } from 'native-base';
import styles from '../../sections/styles/style';

class Loading extends Component {

     componentDidMount(){
        setTimeout( async() => {
        let validationLogin = await AsyncStorage.getItem('userLogin')
        if(validationLogin) {
            validationLogin = JSON.parse(validationLogin)
            if (validationLogin.perm) {
                this.props.navigation.navigate('Inicio')
            }else{
                this.props.navigation.navigate('UserLogin')
            }
        }else{
            //Vista Login
            this.props.navigation.navigate('UserLogin')
        }
    },1000)
}


    render(){
        return(
            <View style={styles.content}>
                <Spinner color="#009688" style={{ width : 200, height : 150}}></Spinner>
            </View>
        )
    }
}

export default Loading