import {StyleSheet} from 'react-native';
import {Constants} from 'expo-constants';

//Constanst permite tomar el tamaño del StatusBar (Android)

const styles = StyleSheet.create({
    container : {
        //00695C ver color si este o no
        backgroundColor: '#77D353',
        paddingTop: 38,
        paddingVertical: 15,
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'white',
        textAlign: 'center',
    }
})

export default styles;