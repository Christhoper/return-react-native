import React, { Component } from 'react';
import { View, Text } from 'native-base';
import { AsyncStorage } from 'react-native';
import styles from '../../sections/styles/style'
import { createAppContainer } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer'
import Inicio2 from '../containers/inicio2'
import API from '../../../utils/api'
import GeoMap from '../../GeoMap/containers/geoMap'
import Agenda from '../../Agenda/containers/agenda'

//Se pueden agregar varios iconos de esta forma import { FontAwesome, Ionicons } from '@expo/vector-icons';
class Inicio extends Component {

    constructor(props){
        super(props);
        this.state = {
            username : '',
            perm : false
        }
    }

    //componentDidMount es ejecutado después de que el componente ha sido montado en el DOM
    async componentDidMount(){
        let data = await API.getData()
        console.log(data)
        //AsyncStorage.removeItem('userLogin')
        // Se creo una promesa para obtener el userLogin de Inicio
        let userLogin = await AsyncStorage.getItem('userLogin')
        // La cadena que se convierte en texto en Inicio la Parseamos a un Obj (JSON.parse)
        userLogin = JSON.parse(userLogin)
        this.setState({ username : userLogin.user, perm : userLogin.perm})
    }

    render() {
        return (
            <View style={styles.content}>
                <Text> Bienvenido {this.state.username}</Text>
            </View>
        )
    }
}

//Crear un SideNav

const drawerNavigator = createDrawerNavigator({
    Inicio: { screen: Inicio },
    Inicio2: { screen: Inicio2 },
    GeoMap : { screen : GeoMap},
    Agenda : { screen : Agenda}
},
{
    //Probar si funcaaaaaaaa
    //Funcionoooo
    initialRouteName : "Inicio",
    unmountInactiveRoutes : true,
    headerMode : "none"
});


export default createAppContainer(drawerNavigator)