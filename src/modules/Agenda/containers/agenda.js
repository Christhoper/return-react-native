import React, { Component } from 'react';
import { View, Text } from 'native-base';
import styles from '../../sections/styles/style'

//Se pueden agregar varios iconos de esta forma import { FontAwesome, Ionicons } from '@expo/vector-icons';
class Agenda extends Component {
    render() {
        return (
            <View style={styles.content}>
                <Text>Hola desde Agenda</Text>
            </View>
        )
    }
}

export default Agenda