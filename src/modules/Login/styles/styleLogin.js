import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({

    textcenter: {
        textAlign: 'center',
        width: '100%'
    },
    content: {
        flex: 1,
        justifyContent: 'center'
    },
    buttonAcceder: {
        marginLeft: '52%'
    },
    buttonRegistrar: {
        marginLeft: '54%'
    },
    body: {
        paddingVertical: 30
    }
})

export default styles