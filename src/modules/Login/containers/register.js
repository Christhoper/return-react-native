import React, { Component } from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input } from 'native-base';
import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import styles from '../styles/styleLogin'

//Se pueden agregar varios iconos de esta forma import { FontAwesome, Ionicons } from '@expo/vector-icons';
class Return extends Component {

    render() {
        return (
            <Container>
                <Content padder contentContainerStyle={styles.content}>
                    <KeyboardAvoidingView behavior="padding" enabled>
                        <Card>
                            <CardItem header bordered>
                                <Text style={styles.textcenter}>Registro de Usuario</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body style={styles.body}>
                                    <Item inlineLabel>
                                        <MaterialIcons name='mail' size={20}></MaterialIcons>
                                        <Input placeholder='Correo electronico' />
                                    </Item>
                                    <Item inlineLabel>
                                        <FontAwesome name='user' size={25}></FontAwesome>
                                        <Input placeholder='Nombre de Usuario' />
                                    </Item>
                                    <Item inlineLabel last>
                                        <FontAwesome name='lock' size={25}></FontAwesome>
                                        <Input placeholder='Contraseña' />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem footer bordered>
                                <Button danger onPress={ () => this.props.navigation.goBack()}>
                                    <Text>Volver</Text>
                                </Button>
                                <Button success style={styles.buttonRegistrar}>
                                    <Text> Registarse </Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </KeyboardAvoidingView>
                </Content>
            </Container>
        );
    }
}

export default Return