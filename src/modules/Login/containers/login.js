import React, { Component } from 'react';
import { KeyboardAvoidingView, StatusBar, AsyncStorage} from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Item, Input } from 'native-base';
import { FontAwesome } from '@expo/vector-icons';
import styles from '../styles/styleLogin'
import Loading from '../../sections/containers/loading'

//Se pueden agregar varios iconos de esta forma import { FontAwesome, Ionicons } from '@expo/vector-icons';
class Return extends Component {

    constructor(props){
        super(props);
        this.state = {
            username : ''
        }
    }

    //Para dejar a una vista con un title especifico
    /*static navigationOptions = ({navigation}) => {
        return {
            header: () => <Header text="RETURN"/>
        }
    }
    */

    navegar = async(param) => {

        let userLogin = {
            user : this.state.username,
            perm : true
        }
        //  JSON.stringify() = combierte un obj a una cadena de texto
        AsyncStorage.setItem('userLogin',  JSON.stringify(userLogin))
        this.props.navigation.navigate(param)

    }
     

    render() {
        return (
            <Container>
                <Content padder contentContainerStyle={styles.content}>
                    <KeyboardAvoidingView behavior="padding" enabled>
                        <StatusBar 
                        barStyle="default" //En Android nos vamos a App.json /* "androidStatusBar" : {   "backgroundColor" : "blue"    }, */
                        // En caso de IOS backgroundColor="blue" y se escribe 
                        >
                        </StatusBar>
                        <Card>
                            <CardItem header bordered>
                                <Text style={styles.textcenter}>Inicio Sesión</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body style={styles.body}>
                                    <Item inlineLabel>
                                        <FontAwesome name='user' size={25}></FontAwesome>
                                        <Input placeholder='Nombre de Usuario' onChangeText = {( username ) => this.setState ( {username} )}/>
                                    </Item>
                                    <Item inlineLabel last>
                                        <FontAwesome name='lock' size={25}></FontAwesome>
                                        <Input placeholder='Contraseña' />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem footer bordered>
                                <Button primary onPress={ () => this.navegar('Register')}>
                                    <Text> Registro </Text>
                                </Button>
                                <Button success style={styles.buttonAcceder} onPress={ () => this.navegar('Inicio')} Loading>
                                    <Text> Ingresar </Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </KeyboardAvoidingView>
                </Content>
            </Container>
        );
    }
}

export default Return