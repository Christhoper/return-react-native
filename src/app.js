import React from 'react';
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { MaterialIcons} from '@expo/vector-icons'
import Login from '../src/modules/Login/containers/login'
import Register from '../src/modules/Login/containers/register'
import Header from './modules/sections/components/header'
import Inicio from './modules/Home/containers/inicio'
import Agenda from './modules/Agenda/containers/agenda'
import Push from './modules/Push/containers/push'
import GeoMap from './modules/GeoMap/containers/geoMap'
import Perfil from './modules/Perfil/containers/perfil'
import Loading from './modules/sections/containers/loading'

//Navegación de las vistas

const LoginNavigator = createStackNavigator({
  Login,
  Register
  //Retornar Components mediante una Función 
}, { defaultNavigationOptions: { header: () => <Header text="RETURN"/>} }),

/*{ headerLayoutPreset: 'center'/* initialRouteName : 'Login' Forzar que vista tiene que mostrar primero 
                                  headerMode : 'none' La vista no tiene Heade */

//Inicio
HomeNavigator = createStackNavigator({
  Inicio
}, { defaultNavigationOptions: { header: () => <Header text="Inicio"/>} })

//Agenda
const AgendaNavigator = createStackNavigator({
  Agenda
}, { defaultNavigationOptions: { header: () => <Header text="Agenda"/>} })

//Notificaciones Push
const PushNavigator = createStackNavigator({
  Push
}, { defaultNavigationOptions: { header: () => <Header text="Notificaciones"/>} })

//Geolocalización
const GeoNavigator = createStackNavigator({
  GeoMap
}, { defaultNavigationOptions: { header: () => <Header text="Ubicación"/>} })

//Perfil
const PerfilNavigator = createStackNavigator({
  Perfil
}, { defaultNavigationOptions: { header: () => <Header text="Perfil"/>} })


const BottomNavigator = createBottomTabNavigator({
  Home : {
    screen : HomeNavigator,
    navigationOptions: {
      title :'Inicio',
      tabBarIcon : ({tintColor}) => <MaterialIcons name="home" color={tintColor} size={24}></MaterialIcons>
    }
  },
  Agenda : {
    screen : AgendaNavigator,
    navigationOptions: {
      title :'Agenda',
      tabBarIcon : ({tintColor}) => <MaterialIcons name="event" color={tintColor} size={24}></MaterialIcons>
    }
  },
  Push : {
    screen : PushNavigator,
    navigationOptions: {
      title :'Notificaciones',
      tabBarIcon : ({tintColor}) => <MaterialIcons name="notifications" color={tintColor} size={24}></MaterialIcons>
    }
  },
  GeoMap : {
    screen : GeoNavigator,
    navigationOptions: {
      title :'Ubicación',
      tabBarIcon : ({tintColor}) => <MaterialIcons name="place" color={tintColor} size={24}></MaterialIcons>
    }
  },
  Perfil : {
    screen : PerfilNavigator,
    navigationOptions: {
      title :'Perfil',
      tabBarIcon : ({tintColor}) => <MaterialIcons name="person" color={tintColor} size={24}></MaterialIcons>
    }
  }
}, { tabBarOptions :  {
  activeTintColor : 'white',
  activeBackgroundColor : '#00695C',
  inactiveTintColor : 'black',
  inactiveBackgroundColor : 'white'

}})

//Se encarga de mostrar las vistas, dependiendo que boton es oprimido (Inicialmente comienza en la vista Login)
const SwitchNavigator = createSwitchNavigator(
  {
    UserLogin: LoginNavigator,
    Loading,
    Home: BottomNavigator,
  },
  {
    initialRouteName: 'Loading'
  }
)

export default createAppContainer(SwitchNavigator) 