//Consumir una API 

const URL = 'https://swapi.co/api/films/'

class API {

    async getData(){
        //Llamada de datos de API a travez de Fetch
        const query = await fetch(URL)
        const data = query.json()
        return data
    }

}

export default new API()